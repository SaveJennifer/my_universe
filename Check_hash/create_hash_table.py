import sys
import os
from config_dir.config import HashTableConfig
from queries.wrapper import SqlWrapper
import logger
import traceback

# HELP:
# scp -r [OBJECT_NAME]/protocol tuz_prod_arflw_sdlr@10.64.210.219:/app/airflow/dags/podft/DEV/

# TODO 3(+): Избавиться от global-переменных. Использовать конфиг и класс


def get_describe_txt(system_alias: str,
					 database_name: str,
					 table_name: str):
	"""
	Подключение по SSH к кластеру. 
	Выполнение запроса к Hive.
	Сохранение результата в файле на том сервере, где запускается python-скрипт/
	"""
	try:
		cfg = HashTableConfig(
			system_alias=system_alias,
    	    table_name=table_name
		)

		# TODO 3: Кривой вывод в консоли

		if not os.path.exists(cfg.DESCRIBE_HASH_TABLE_DIR):
			os.mkdir(cfg.DESCRIBE_HASH_TABLE_DIR)
			logger.write_log(message="Describe file\'s directory was created", log_type=0, prefix='PODFT_HASH')
		os.system('ssh {HADOOP_SERVER} | hive -e "use {database_name}; DESCRIBE {table_name}" > {DESCRIBE_HASH_TABLE_FILEPATH}'.format(
			HADOOP_SERVER=cfg.HADOOP_SERVER, 
			database_name=database_name, 
			table_name=table_name, 
			DESCRIBE_HASH_TABLE_FILEPATH=cfg.DESCRIBE_HASH_TABLE_FILEPATH)
		)
		logger.write_log(message="Desribe file was created", log_type=0, prefix='PODFT_HASH')
		
		return None

	except Exception as e:
		logger.write_log(message=traceback.format_exc(), log_type=1, prefix='PODFT_HASH')
	


def parse_describe(table_name: str,
				   system_alias: str):
	"""
	Читает describe-файл.
	Каждую строку в файле приводим к типу кортеж, вид (<ИМЯ_КОЛОНКИ>, <ТИП_КОЛОНКИ>).
	Из полученных кортежей формируем список кортежей вида [<КОРТЕЖ_1>, <КОРТЕЖ_2>, ...].
	Полученный список сохраняем в переменную col_type_result и возвращаем. В последствии она пойдет на вход функции create_hash_queries
	"""
	try:
		cfg = HashTableConfig(
			system_alias=system_alias,
    	    table_name=table_name
		)

		with open(cfg.DESCRIBE_HASH_TABLE_FILEPATH, 'r') as f:
			col_type_result = []
			for line in f:
				col_type_result.append(tuple(line.split()))

		return col_type_result

	except Exception as e:
		logger.write_log(message=traceback.format_exc(), log_type=1, prefix='PODFT_HASH')
	
def create_hash_queries(table_name: str,
						col_x_types: list,
						table_key_list: list,
						sur_key_list: list,
						system_alias: str):
	
	"""
	Из списка отношений <КОЛОНКА-ТИП> генерируется запрос, создающий хэш-таблицу.
	Логика проверок:
		Если колонка является ключом, она не хэшируется;
		Если колонка является строкой, в ней удаляется символ '#';
		Если колонка является DECIMAL, она преобразуется в строку;
		Если колонка является TINYINT, она преобразуется в строку;
		Если колонка является TIMESTAMP, она преобразуется в строку;
		Если колонка не попадает в типы, описанные выше, она преобразуется в строку и в ней удаляется символ '#'. Информация о такой колонке сохраняется в логе.
	На основе колонок и их типов создается SELECT-запрос для формирования хэш-таблицы. 
	Позднее на основе этого запроса будут создана (CREATE TABLE AS SELECT) и наполнена (INSERT INTO TABLE SELECT) хэш-таблица.
	Создай запросы CREATE и INSERT. Сохрани их в sql-скрипты (отдельные) в директорию result/<table_name>/CREATE_HASH_TABLE.sql и INSERT_HASH_TABLE.sql
	Синтаксис запроса: Hive SQL.
	Директория, где хранятся сгенерированные запросы: RESULT_DIR.
	"""
	try:
		cfg = HashTableConfig(
			system_alias=system_alias,
    	    table_name=table_name
		)

		hash_exception_column_list = table_key_list + sur_key_list

		# Проверка, что директории нет
		if not os.path.exists(cfg.RESULT_HASH_TABLE_DIR):
			os.mkdir(cfg.RESULT_HASH_TABLE_DIR)
			logger.write_log(message="Result hash table directory was created", log_type=0, prefix='PODFT_HASH')

		hash_query = "SELECT\n\t{key_fields},\n\tMD5(CONCAT(".format(key_fields=',\n\t'.join(table_key_list))


		cast_as_type_dict = {
			'decimal': "\n\t\tCOALESCE(CAST({0} as string),'')",
			'string': "\n\t\tCOALESCE(REPLACE({0},'#',''),'')",
			'timestamp':"\n\t\tCOALESCE(CAST({0} as string),'')",
			'int':"\n\t\tCOALESCE(CAST({0} as string),'')",
			'tinyint':"\n\t\tCOALESCE(CAST({0} as string),'')",
			'other': "\n\t\tCOALESCE(REPLACE(CAST({0} as string),'#',''),'')"
		}
		for idx, col in enumerate(col_x_types):
			column_name = col[0]
			# Отсекаем от типа все, что правее открывающейся скобки
			column_type = col[1].split('(')[0]


			# Убираем возможные дубли из hash_exception_column_list через set и приводим все элементы к нижнему регистру
			if column_name.lower() not in set(map(lambda exception_column: exception_column.lower(), hash_exception_column_list)):
				if column_type not in cast_as_type_dict.keys():
					print("Unexpected column type '{0}' for column '{1}'".format(column_type.upper(), column_name.upper()))
					column_type = 'other'
				hash_query += cast_as_type_dict[column_type].format(column_name)

				if idx != len(col_x_types) - 1:
					hash_query += ",'#',"
				else:
					hash_query += "\n\t)\n) AS hash_value FROM {database_name}.{table_name}".format(
						database_name=cfg.DATABASE_WRK, 
						table_name=table_name
					)

		select_wrapper = SqlWrapper(
			database_name='{database_name}',
			target_table_name = '{table_name}', 
			query=hash_query
		)

		create_select_wrapper_sql = select_wrapper.wrap_as_create()
		insert_select_wrapper_sql = select_wrapper.wrap_as_insert()

		create_table_wrapper=create_select_wrapper_sql.format(
			database_name=cfg.DATABASE_WRK,
			table_name="{table_name}_{system_alias}_HASH".format(table_name=table_name, system_alias=system_alias)
		)
		# TODO 2: Уведомление в лог, что запрос <какой-то> создан и сохранен в <путь_с_именем_файла>
		with open (cfg.SQL_CREATE_TABLE_FILEPATH, 'w+') as sql_create_table:
			sql_create_table.write(create_table_wrapper)
			logger.write_log(message="Create file of {table_name} was created".format(table_name=table_name), log_type=0, prefix='PODFT_HASH')

		insert_wrapper=insert_select_wrapper_sql.format(
			database_name=cfg.DATABASE_WRK,
			table_name="{table_name}_{system_alias}_HASH".format(table_name=table_name, system_alias=system_alias)
		)

		with open (cfg.SQL_INSERT_FILEPATH, 'w+') as sql_insert:
			sql_insert.write(insert_wrapper)
			logger.write_log(message="Insert file of {table_name} was created".format(table_name=table_name), log_type=0, prefix='PODFT_HASH')

		return None

	except Exception as e:
		logger.write_log(message=traceback.format_exc(), log_type=1, prefix='PODFT_HASH')

def build_hash_table():

	# Создается хэш-таблица: выполняется скрипт {RESULT_HASH_TABLE_DIR}/{CREATE_TABLE_FILENAME_SQL}
	# Хэш-таблица наполняется данными: {RESULT_HASH_TABLE_DIR}/{INSERT_FILENAME_SQL}
	# Выполняется через spark-submit. Пока что делаем руками.
	
	return None
	
	
def main(database_name: str,
		 table_name: str,
		 table_key_list: list,
		 sur_key_list: list,
		 system_alias: str):
		
	# get_describe_txt достаточно прогнать 1 раз для 1 таблицы
	#get_describe_txt(system_alias=system_alias,
	#				 database_name=database_name,
	#				 table_name=table_name)
	parsed_describe = parse_describe(table_name=table_name,
									 system_alias=system_alias)
	
	create_hash_queries(table_name=table_name,
						col_x_types=parsed_describe, 
						table_key_list=table_key_list,
						sur_key_list=sur_key_list,
						system_alias=system_alias)
	#build_hash_table()
				 
	
	

if __name__ == '__main__':

	database_name = sys.argv[1]
	table_name = sys.argv[2]	
	table_key_list = sys.argv[3].split(' ')
	sur_key_list = sys.argv[4].split(' ')
	system_alias = sys.argv[5]
	
	main(database_name=database_name,
		 table_name=table_name,
		 table_key_list=table_key_list,
		 sur_key_list=sur_key_list,
		 system_alias=system_alias)
