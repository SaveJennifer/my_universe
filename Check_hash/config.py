class MainConfig:

	def __init__(self):

		self.HOME_DIR = '/u00/home/sas/sasetl/podft_test/CHECK_HASH'
		self.DESCRIBE_DIR = self.HOME_DIR + '/describe'
		self.LOGS_DIR = self.HOME_DIR + '/logs'
		

		self.DRP_HADOOP_SERVER = 'bda3node06' 
		# DAPP_HADOOP_SERVER = DRP_HADOOP_SERVER для отладки. В целевой картине значение DAPP_HADOOP_SERVER соответствует адресу кластера DAPP
		self.DAPP_HADOOP_SERVER = 'bda3node06'
		self.DATABASE_WRK = 'UAT_DM_AML_WRK'
		self.DRP_HASH_ALIAS = "DRP_HASH"
		self.DAPP_HASH_ALIAS = "DAPP_HASH"

class CompareTablesConfig(MainConfig):

	def __init__(self, 
				 dapp_hash_table_name: str,
				 drp_hash_table_name: str):

		super().__init__()
		
		self.RESULT_DIR = self.HOME_DIR + '/compare_result'
		self.RESULT_COMPARE_HASH_TABLES_DIR = "{RESULT_DIR}/{dapp_hash_table_name}".format(RESULT_DIR=self.RESULT_DIR, 
																				  dapp_hash_table_name=dapp_hash_table_name)
		self.CREATE_TABLE_DRP_KEY_DIFF_FILENAME_SQL = "{drp_hash_table_name}_DIFF_CREATE_TABLE.sql".format(drp_hash_table_name=drp_hash_table_name)
		self.INSERT_DRP_KEY_DIFF_FILENAME_SQL = "{drp_hash_table_name}_DIFF_INSERT.sql".format(drp_hash_table_name=drp_hash_table_name)
		self.CREATE_TABLE_DAPP_KEY_DIFF_FILENAME_SQL = "{dapp_hash_table_name}_DIFF_CREATE_TABLE.sql".format(dapp_hash_table_name=dapp_hash_table_name)
		self.INSERT_DAPP_KEY_DIFF_FILENAME_SQL = "{dapp_hash_table_name}_DIFF_INSERT.sql".format(dapp_hash_table_name=dapp_hash_table_name)
		self.CREATE_TABLE_HASH_DIFF_FILENAME_SQL = "{drp_hash_table_name}_HASH_DIFF_CREATE_TABLE.sql".format(drp_hash_table_name=drp_hash_table_name)
		self.INSERT_HASH_DIFF_FILENAME_SQL = "{drp_hash_table_name}_HASH_DIFF_INSERT.sql".format(drp_hash_table_name=drp_hash_table_name)

		self.SQL_CREATE_TABLE_DRP_KEY_DIFF_FILEPATH = "{RESULT_COMPARE_HASH_TABLES_DIR}/{CREATE_TABLE_DRP_KEY_DIFF_FILENAME_SQL}".format(
			RESULT_COMPARE_HASH_TABLES_DIR=self.RESULT_COMPARE_HASH_TABLES_DIR,
			CREATE_TABLE_DRP_KEY_DIFF_FILENAME_SQL=self.CREATE_TABLE_DRP_KEY_DIFF_FILENAME_SQL
		)

		self.SQL_INSERT_DRP_KEY_DIFF_FILEPATH = "{RESULT_COMPARE_HASH_TABLES_DIR}/{INSERT_DRP_KEY_DIFF_FILENAME_SQL}".format(
			RESULT_COMPARE_HASH_TABLES_DIR=self.RESULT_COMPARE_HASH_TABLES_DIR,
			INSERT_DRP_KEY_DIFF_FILENAME_SQL=self.INSERT_DRP_KEY_DIFF_FILENAME_SQL
		)

		self.SQL_CREATE_TABLE_DAPP_KEY_DIFF_FILEPATH = "{RESULT_COMPARE_HASH_TABLES_DIR}/{CREATE_TABLE_DAPP_KEY_DIFF_FILENAME_SQL}".format(
			RESULT_COMPARE_HASH_TABLES_DIR=self.RESULT_COMPARE_HASH_TABLES_DIR,
			CREATE_TABLE_DAPP_KEY_DIFF_FILENAME_SQL=self.CREATE_TABLE_DAPP_KEY_DIFF_FILENAME_SQL
		)

		self.SQL_INSERT_DAPP_KEY_DIFF_FILEPATH = "{RESULT_COMPARE_HASH_TABLES_DIR}/{INSERT_DAPP_KEY_DIFF_FILENAME_SQL}".format(
			RESULT_COMPARE_HASH_TABLES_DIR=self.RESULT_COMPARE_HASH_TABLES_DIR,
			INSERT_DAPP_KEY_DIFF_FILENAME_SQL=self.INSERT_DAPP_KEY_DIFF_FILENAME_SQL
		)

		self.SQL_CREATE_TABLE_HASH_DIFF_FILEPATH = "{RESULT_COMPARE_HASH_TABLES_DIR}/{CREATE_TABLE_HASH_DIFF_FILENAME_SQL}".format(
			RESULT_COMPARE_HASH_TABLES_DIR=self.RESULT_COMPARE_HASH_TABLES_DIR,
			CREATE_TABLE_HASH_DIFF_FILENAME_SQL=self.CREATE_TABLE_HASH_DIFF_FILENAME_SQL
		)

		self.SQL_INSERT_HASH_DIFF_FILEPATH = "{RESULT_COMPARE_HASH_TABLES_DIR}/{INSERT_HASH_DIFF_FILENAME_SQL}".format(
			RESULT_COMPARE_HASH_TABLES_DIR=self.RESULT_COMPARE_HASH_TABLES_DIR,
			INSERT_HASH_DIFF_FILENAME_SQL=self.INSERT_HASH_DIFF_FILENAME_SQL
		)

		


class HashTableConfig(MainConfig):

	def __init__(self, 
				 system_alias: str,
				 table_name: str):

		super().__init__()

		self.RESULT_DIR = self.HOME_DIR + '/hash_result'

		self.DESCRIBE_HASH_TABLE_DIR = "{DESCRIBE_DIR}/{table_name}".format(
		DESCRIBE_DIR=self.DESCRIBE_DIR,
		table_name=table_name
		)

		self.DESCRIBE_HASH_TABLE_FILEPATH = "{DESCRIBE_HASH_TABLE_DIR}/{table_name}_{system_alias}.txt".format(
		DESCRIBE_HASH_TABLE_DIR=self.DESCRIBE_HASH_TABLE_DIR, 
		table_name=table_name, 
		system_alias=system_alias
		)

		self.RESULT_HASH_TABLE_DIR = "{RESULT_DIR}/{table_name}".format(
		RESULT_DIR=self.RESULT_DIR,
		table_name=table_name
		)
		
		self.CREATE_TABLE_FILENAME_SQL = "{table_name}_HASH_CREATE_TABLE.sql".format(table_name=table_name)
		self.INSERT_FILENAME_SQL = "{table_name}_HASH_INSERT.sql".format(table_name=table_name)

		self.SQL_CREATE_TABLE_FILEPATH = "{RESULT_HASH_TABLE_DIR}/{CREATE_TABLE_FILENAME_SQL}".format(
			RESULT_HASH_TABLE_DIR=self.RESULT_HASH_TABLE_DIR,
			CREATE_TABLE_FILENAME_SQL=self.CREATE_TABLE_FILENAME_SQL
		)

		self.SQL_INSERT_FILEPATH = "{RESULT_HASH_TABLE_DIR}/{INSERT_FILENAME_SQL}".format(
			RESULT_HASH_TABLE_DIR=self.RESULT_HASH_TABLE_DIR,
			INSERT_FILENAME_SQL=self.INSERT_FILENAME_SQL
		)

		if system_alias == 'DRP':
			self.HADOOP_SERVER = self.DRP_HADOOP_SERVER
		elif system_alias == 'DAPP':
			self.HADOOP_SERVER = self.DAPP_HADOOP_SERVER
		else:
			raise ValueError("Wrong system_alias = {system_alias}. Availiable values: ['DRP', 'DAPP']".format(system_alias=system_alias))
		
		

