import os
import sys
import time
import json
import traceback 
from functools import wraps
from datetime import datetime
from config_dir.config import MainConfig

conf = MainConfig()
#print("[DEBUG] -- logger -- {conf}".format(conf=conf))

logs_folder = conf.LOGS_DIR
log_types = {0: 'Info', 1: 'Error', 2:'Warning', 3:'Debug'}


def write_log(message, log_type, prefix, params=None, retry_cnt=5):
    try:
        if not os.path.exists(logs_folder):
            os.makedirs(logs_folder)
        log_name = prefix + '_' + datetime.today().strftime('%Y-%m-%d_%H:%M:%S') + ".log"
        log_path = logs_folder + '/' + log_name
        
        traceback_str = ''
        if (isinstance(message, Exception)):
            traceback_str = str(traceback.format_exc())
            
        message = str(message)
        log_type = log_types[log_type]
        
        params1_str = ''
        if params is not None:            
            for key, value in params.items():
                    params1_str += '\t' + str(key) + ' = ' + str(value) + '\r\n'

        log_str = '-' * 99 + '\r\n' \
        + 'Date: ' + datetime.today().strftime('%Y-%m-%d %H:%M:%S') + '\r\n\r\n' \
        + 'Type log: ' + log_type + '\r\n' \
        + 'Text log: ' + message + '\r\n' \
        + 'Used parameters:\r\n' \
        + params1_str + '\r\n' \
        + traceback_str \
        + '\r\n'
        
        #print(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
        #print("LOG_INFO: ", log_str.encode('utf-8'))
        #print(log_type)

        while write_log_sync(log_path, log_str) == False and retry_cnt != 0:
            time.sleep(1)
            retry_cnt -= 1
        if retry_cnt == 0:
            raise Exception
            
    except Exception:
        print(traceback.format_exc())


def write_log_sync(log_path, log_str):
    try:
        f = open(log_path, 'a', encoding='utf-8')
        f.write(log_str)
        f.close()
        return True
    except Exception:
        return False


def refresh_log_directory(storage_days=14):
    try:
        storage_days_utime = storage_days * 24 * 60 * 60
        remove_date = time.time() - storage_days_utime
        for log in os.listdir(logs_folder):
            if log.lower().endswith('.log'):
                stat_log = os.stat(logs_folder + log)
                create_date = stat_log.st_mtime
                if create_date < remove_date:
                    os.unlink(logs_folder + log)
    except Exception as ex:
        tried_params = {
            'function': 'refresh_log_directory'
        }
        write_log(ex, log_type=2, params=tried_params)


# Декоратор над декоратором используется для возможности передачи параметра log_type
def logger(log_type=0):
    def logger_decorator(func):  # func - любая функция на которой мы используем декоратор
        # необходимо, чтобы имя функции в Exception было func.__name__
        # без использования wraps будет использоваться имя декоратора
        @wraps(func) 
        def wrapper(*args, **kwargs):
            try:
                res = func(*args, **kwargs)
                return res
            except Exception as ex:
                tried_params = {
                'function': func.__name__,
                'args': args
                }
                write_log(ex, log_type=log_type, params=tried_params, prefix=__name__)
                if log_type == 1:
                    exit(1)
        return wrapper 
    return logger_decorator