SELECT_KEY_DIFF_TEMPLATE_SQL = """
SELECT {key_fields_select_statement}
    {master_table_alias}.HASH_VALUE
FROM {database_name}.{master_table_name} AS {master_table_alias}
LEFT JOIN {database_name}.{slave_table_name} AS {slave_table_alias}
ON {on_statement_condition}"""


SELECT_HASH_DIFF_TEMPLATE_SQL = """
SELECT {key_fields_select_statement}
    {dapp_hash_table_name}.HASH_VALUE AS HASH_VALUE_DAPP,
    {drp_hash_table_name}.HASH_VALUE AS HASH_VALUE_DRP
FROM {database_name}.{dapp_hash_table_name} AS {dapp_table_alias}
INNER JOIN {database_name}.{drp_hash_table_name} AS {drp_table_alias}
ON {on_statement_condition}"""