class SqlWrapper:

	def __init__(self,
				 database_name:str,
				 target_table_name:str, 
				 query:str):

		self.database_name = database_name
		self.target_table_name = target_table_name
		self.query = query


	def wrap_as_create(self):

		if "WHERE" not in self.query:
			result_query = """
CREATE TABLE """ + self.database_name + """.""" + self.target_table_name + """ AS """ + self.query + """
WHERE 1=0"""
		else:
			result_query = """
CREATE TABLE """ + self.database_name + """.""" + self.target_table_name + """ AS """ + self.query + """
AND 1=0"""

		return result_query


	def wrap_as_insert(self):
		result_query =  """
INSERT INTO TABLE """ + self.database_name + """.""" + self.target_table_name + """ """ + self.query

		return result_query