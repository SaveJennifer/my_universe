import sys
import os
from config_dir.config import CompareTablesConfig
from queries.templates import SELECT_KEY_DIFF_TEMPLATE_SQL, SELECT_HASH_DIFF_TEMPLATE_SQL
from queries.wrapper import SqlWrapper
import logger
import traceback

def create_key_diff(drp_hash_table_name: str,
					dapp_hash_table_name: str,
					table_key_list: list):
	try:
		cfg = CompareTablesConfig(drp_hash_table_name=drp_hash_table_name,
								  dapp_hash_table_name=dapp_hash_table_name)
		
		# Проверка, что директории нет
		if not os.path.exists(cfg.RESULT_COMPARE_HASH_TABLES_DIR):
			os.mkdir(cfg.RESULT_COMPARE_HASH_TABLES_DIR)
			logger.write_log(message='{RESULT_COMPARE_HASH_TABLES_DIR} directory for compare result\'s files was created'.format(RESULT_COMPARE_HASH_TABLES_DIR=cfg.RESULT_COMPARE_HASH_TABLES_DIR), log_type=0, prefix='PODFT_COMPARE')
		
		on_statement_condition = "1=1"
		where_statement_insert_condition = "1=1"
		key_fields_select_statement = ""
		for table_key in table_key_list:
			key_fields_select_statement += "\n\t{{master_table_alias}}.{table_key},".format(table_key=table_key)
			on_statement_condition += "\n\tAND {{master_table_alias}}.{table_key} = {{slave_table_alias}}.{table_key}".format(table_key=table_key)
			where_statement_insert_condition += "\n\tAND {{slave_table_alias}}.{table_key} IS NULL".format(table_key=table_key)
		
		key_diff_wrapper = SqlWrapper(
			database_name='{database_name}',
			target_table_name = '{master_table_name}_DIFF', 
			query=SELECT_KEY_DIFF_TEMPLATE_SQL
		)
		
	
		create_key_diff_template_sql = key_diff_wrapper.wrap_as_create()
		insert_key_diff_template_sql = key_diff_wrapper.wrap_as_insert()
	
	
		create_hash_query_drp_key_diff = create_key_diff_template_sql.format(
			database_name=cfg.DATABASE_WRK,
			master_table_name=drp_hash_table_name + "_DRP_HASH",
			master_table_alias=cfg.DRP_HASH_ALIAS,
			slave_table_name=dapp_hash_table_name + "_DAPP_HASH",
			slave_table_alias=cfg.DAPP_HASH_ALIAS,
			key_fields_select_statement=key_fields_select_statement.format(
				master_table_alias=cfg.DRP_HASH_ALIAS
			),											 
			on_statement_condition=on_statement_condition.format(
				master_table_alias=cfg.DRP_HASH_ALIAS, 
				slave_table_alias=cfg.DAPP_HASH_ALIAS
			)
		)
	
	
		with open(cfg.SQL_CREATE_TABLE_DRP_KEY_DIFF_FILEPATH, 'w+') as sql_create_drp_key_diff:
			sql_create_drp_key_diff.write(create_hash_query_drp_key_diff)
			logger.write_log(message='Create file with the difference of the key fields in {drp_hash_table_name} was created'.format(drp_hash_table_name=drp_hash_table_name), log_type=0, prefix='PODFT_COMPARE')
	
																	 
		insert_hash_query_drp_key_diff = insert_key_diff_template_sql.format(
			database_name=cfg.DATABASE_WRK,
			master_table_name=drp_hash_table_name + "_DRP_HASH",
			master_table_alias=cfg.DRP_HASH_ALIAS,
			slave_table_name=dapp_hash_table_name + "_DAPP_HASH",
			slave_table_alias=cfg.DAPP_HASH_ALIAS,
			key_fields_select_statement=key_fields_select_statement.format(
				master_table_alias=cfg.DRP_HASH_ALIAS
			),
			on_statement_condition=on_statement_condition.format(
				master_table_alias=cfg.DRP_HASH_ALIAS, 
				slave_table_alias=cfg.DAPP_HASH_ALIAS
			),
			where_statement_insert_condition=where_statement_insert_condition.format(
				slave_table_alias=cfg.DAPP_HASH_ALIAS
			)
		)
	
	
		with open(cfg.SQL_INSERT_DRP_KEY_DIFF_FILEPATH, 'w+') as sql_insert_drp_key_diff:
			sql_insert_drp_key_diff.write(insert_hash_query_drp_key_diff)
			logger.write_log(message='Insert file with the difference of the key fields in {drp_hash_table_name} was created'.format(drp_hash_table_name=drp_hash_table_name), log_type=0, prefix='PODFT_COMPARE')
	
	
		create_hash_query_dapp_key_diff = create_key_diff_template_sql.format(
			database_name=cfg.DATABASE_WRK,
			master_table_name=dapp_hash_table_name + "_DAPP_HASH",
			master_table_alias=cfg.DAPP_HASH_ALIAS,
			slave_table_name=drp_hash_table_name,
			slave_table_alias=cfg.DRP_HASH_ALIAS + "_DRP_HASH",
			key_fields_select_statement=key_fields_select_statement.format(
				master_table_alias=cfg.DAPP_HASH_ALIAS
			),
			on_statement_condition=on_statement_condition.format(
				master_table_alias=cfg.DAPP_HASH_ALIAS,
				slave_table_alias=cfg.DRP_HASH_ALIAS
			)
		)


		with open(cfg.SQL_CREATE_TABLE_DAPP_KEY_DIFF_FILEPATH, 'w+') as sql_create_table_dapp_key_diff:
			sql_create_table_dapp_key_diff.write(create_hash_query_dapp_key_diff)
			logger.write_log(message='Create file with the difference of the key fields in {dapp_hash_table_name} was created'.format(dapp_hash_table_name=dapp_hash_table_name), log_type=0, prefix='PODFT_COMPARE')

		insert_hash_query_dapp_key_diff = insert_key_diff_template_sql.format(
			database_name=cfg.DATABASE_WRK,
			master_table_name=dapp_hash_table_name,
			master_table_alias=cfg.DAPP_HASH_ALIAS + "_DAPP_HASH",
			slave_table_name=drp_hash_table_name,
			slave_table_alias=cfg.DRP_HASH_ALIAS + "_DRP_HASH",
			key_fields_select_statement=key_fields_select_statement.format(
				master_table_alias=cfg.DAPP_HASH_ALIAS
			),
			on_statement_condition=on_statement_condition.format(
				master_table_alias=cfg.DAPP_HASH_ALIAS, 
				slave_table_alias=cfg.DRP_HASH_ALIAS
			),
			where_statement_insert_condition=where_statement_insert_condition.format(
				slave_table_alias=cfg.DRP_HASH_ALIAS
			)																	 
		)																 

	
		with open(cfg.SQL_INSERT_DAPP_KEY_DIFF_FILEPATH, 'w+') as sql_insert_dapp_key_diff:
			sql_insert_dapp_key_diff.write(insert_hash_query_dapp_key_diff)
			logger.write_log(message='Insert file with the difference of the key fields in {dapp_hash_table_name} was created'.format(dapp_hash_table_name=dapp_hash_table_name), log_type=0, prefix='PODFT_COMPARE')
		return None
	except Exception as e:
		logger.write_log(message=traceback.format_exc(), log_type=1, prefix='PODFT_COMPARE')





def create_hash_diff(drp_hash_table_name: str,
					 dapp_hash_table_name: str,
					 table_key_list: list):
	

	try:
		cfg = CompareTablesConfig(drp_hash_table_name=drp_hash_table_name,
								  dapp_hash_table_name=dapp_hash_table_name)



		on_statement_condition = "1=1"
		where_statement_insert_condition = "1=1"
		key_fields_select_statement = ""
		for table_key in table_key_list:
			key_fields_select_statement += "\n\t{{dapp_table_alias}}.{table_key},".format(table_key=table_key)
			on_statement_condition += "\n\tAND {{dapp_table_alias}}.{table_key} = {{drp_table_alias}}.{table_key}".format(table_key=table_key)
			where_statement_insert_condition += "\n\tAND {{drp_table_alias}}.hash_value <> {{dapp_table_alias}}.hash_value".format(table_key=table_key)


		hash_diff_wrapper = SqlWrapper(
			database_name='{database_name}',
			target_table_name = '{dapp_hash_table_name}_DIFF', 
			query=SELECT_HASH_DIFF_TEMPLATE_SQL
		)


		create_hash_diff_template_sql = hash_diff_wrapper.wrap_as_create()
		insert_hash_diff_template_sql = hash_diff_wrapper.wrap_as_insert()


		create_hash_query_hash_diff = create_hash_diff_template_sql.format(
			database_name=cfg.DATABASE_WRK,
			dapp_hash_table_name=dapp_hash_table_name + "_DAPP_HASH",
			dapp_table_alias=cfg.DAPP_HASH_ALIAS,
			drp_hash_table_name=drp_hash_table_name + "_DRP_HASH",
			drp_table_alias=cfg.DRP_HASH_ALIAS,
			key_fields_select_statement=key_fields_select_statement.format(
				dapp_table_alias=cfg.DAPP_HASH_ALIAS
			),
			on_statement_condition=on_statement_condition.format(
				dapp_table_alias=cfg.DAPP_HASH_ALIAS, 
				drp_table_alias=cfg.DRP_HASH_ALIAS
			)
		)


		with open(cfg.SQL_CREATE_TABLE_HASH_DIFF_FILEPATH, 'w+') as sql_create_table_hash_diff:
			sql_create_table_hash_diff.write(create_hash_query_hash_diff)
			logger.write_log(message='Create file with the difference of the hash fields in {dapp_hash_table_name} was created'.format(dapp_hash_table_name=dapp_hash_table_name), log_type=0, prefix='PODFT_COMPARE')

		insert_hash_query_hash_diff = insert_hash_diff_template_sql.format(
			database_name=cfg.DATABASE_WRK,
			dapp_hash_table_name=dapp_hash_table_name + "_DAPP_HASH",
			dapp_table_alias=cfg.DAPP_HASH_ALIAS,
			drp_hash_table_name=drp_hash_table_name + "_DRP_HASH",
			drp_table_alias=cfg.DRP_HASH_ALIAS,
			key_fields_select_statement=key_fields_select_statement.format(
				dapp_table_alias=cfg.DAPP_HASH_ALIAS
			),
			on_statement_condition=on_statement_condition.format(
				dapp_table_alias=cfg.DAPP_HASH_ALIAS, 
				drp_table_alias=cfg.DRP_HASH_ALIAS
			),
			where_statement_insert_condition=where_statement_insert_condition.format(
				drp_table_alias=cfg.DRP_HASH_ALIAS, 
				dapp_table_alias=cfg.DAPP_HASH_ALIAS
			)
		)																 


		with open(cfg.SQL_INSERT_HASH_DIFF_FILEPATH, 'w+') as sql_insert_hash_diff:
			sql_insert_hash_diff.write(insert_hash_query_hash_diff)
			logger.write_log(message='Insert file with the difference of the hash fields in {dapp_hash_table_name} was created'.format(dapp_hash_table_name=dapp_hash_table_name), log_type=0, prefix='PODFT_COMPARE')

		return None
	except Exception as e:
		logger.write_log(message=traceback.format_exc(), log_type=1, prefix='PODFT_COMPARE')


	
def main(drp_hash_table_name: str,
		 dapp_hash_table_name: str,
		 table_key_list: list):
		
	create_key_diff(drp_hash_table_name,
					dapp_hash_table_name=dapp_hash_table_name, 
					table_key_list=table_key_list)				 
	create_hash_diff(drp_hash_table_name,
	 				 dapp_hash_table_name=dapp_hash_table_name, 
					 table_key_list=table_key_list)	
	

if __name__ == '__main__':

	drp_hash_table_name = sys.argv[1]
	dapp_hash_table_name = sys.argv[2]	
	table_key_list = sys.argv[3].split(' ')
	
	main(drp_hash_table_name=drp_hash_table_name,
		 dapp_hash_table_name=dapp_hash_table_name,
		 table_key_list=table_key_list)

