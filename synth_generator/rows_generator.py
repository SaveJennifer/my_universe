import random
from config import SynthTableConfig



def generate_row(config: SynthTableConfig) -> list:

    """

    Parameters
    ----------
    config: SynthTableConfig
    Returns
    -------
    list
    """

    import pandas as pd
    from src.lib.value_generator import generate_value

    pd.set_option('mode.chained_assignment', None)

    row = []
    for column in range(config.COLUMN_NUM):
        column_type = config.COLUMN_TYPE_DICT[column]
        param_name = config.PARAM_NAME_DICT[column]
        param_value = config.PARAM_VALUE_DICT[column]
        column_data_domain = config.COLUMN_DATA_DOMAIN_DICT[column]

        generated_value = generate_value(
            config=config,
            column_type=column_type,
            column_data_domain=column_data_domain,
            param_name=param_name,
            param_value=param_value
        )

        row.append(generated_value)

    return row


def close_version(
        row: list,
        config: SynthTableConfig) -> list:

    """

    Parameters
    ----------
    row: list
    config: SynthTableConfig

    Returns
    -------
    list
    """

    from src.lib.data_domains import close_effective_to_dttm

    close_effective_to_dttm_val = None

    for column in range(config.COLUMN_NUM):
        if config.COLUMN_DATA_DOMAIN_DICT[column] == 'ACTIVE_FLG':
            row[column] = 0
        if config.COLUMN_DATA_DOMAIN_DICT[column] == 'FROM_DTTM':
            close_effective_to_dttm_val = close_effective_to_dttm(
                config=config,
                start_date=row[column]
            )
        if config.COLUMN_DATA_DOMAIN_DICT[column] == 'TO_DTTM':
            if close_effective_to_dttm_val:
                row[column] = close_effective_to_dttm_val
            # TODO: Костыль
            else:
                for eff_from_lookup in range(config.COLUMN_NUM):
                    if config.COLUMN_DATA_DOMAIN_DICT[eff_from_lookup] == 'FROM_DTTM':
                        close_effective_to_dttm_val = close_effective_to_dttm(
                            config=config,
                            start_date=row[column]
                        )

    return row


def generate_new_version(
        row_previous_version: list,
        config: SynthTableConfig) -> list:

    """

    Parameters
    ----------
    row_previous_version: list
    config: SynthTableConfig

    Returns
    -------
    list
    """

    from datetime import timedelta
    from src.lib.data_domains import open_effective_to_dttm
    from src.lib.value_generator import generate_value

    # Generate new version based on previous version
    row_new_version = row_previous_version.copy()

    scd2_mutable_columns = []
    for column_id in range(config.COLUMN_NUM):
        if config.COLUMN_ROLE_DICT[column_id] not in config.IMMUTABLE_SCD2_ROLES:
            scd2_mutable_columns.append(column_id)

    column_id_to_change = random.choice(scd2_mutable_columns)

    for column_id in range(config.COLUMN_NUM):

        column_type = config.COLUMN_TYPE_DICT[column_id]
        param_name = config.PARAM_NAME_DICT[column_id]
        param_value = config.PARAM_VALUE_DICT[column_id]
        column_data_domain = config.COLUMN_DATA_DOMAIN_DICT[column_id]

        if column_data_domain == 'ACTIVE_FLG':
            row_new_version[column_id] = 1
        if column_data_domain == 'FROM_DTTM':
            row_new_version[column_id] = row_new_version[column_id] + timedelta(seconds=1)
        if column_data_domain == 'TO_DTTM':
            row_new_version[column_id] = open_effective_to_dttm(config=config)
        if column_id == column_id_to_change:
            generated_value = row_new_version[column_id]

            while generated_value == row_new_version[column_id]:
                generated_value = generate_value(
                    config=config,
                    column_type=column_type,
                    column_data_domain=column_data_domain,
                    param_name=param_name,
                    param_value=param_value
                )
            row_new_version[column_id] = generated_value


    return row_new_version
