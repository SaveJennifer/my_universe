import datetime
from config import SynthTableConfig


def pkrk(
        config: SynthTableConfig,
        **kwargs) -> int:

    """

    Parameters
    ----------
    config: SynthTableConfig,

    Returns
    -------
    int
    """

    return config.CURRENT_GENERATE_ROW_NUMBER


def strdigit(
        config: SynthTableConfig,
        param_name: str,
        param_value: str,
        **kwargs) -> str:

    """

    Parameters
    ----------
     config: SynthTableConfig,
    param_name: str
    param_value: str
    Returns
    -------
    str or int
    """

    import random
    import string

    if param_name == 'LENGTH':
        return ''.join(random.choice(string.digits) for _ in range(int(param_value)))

    elif param_name == 'VALUE':
        return str(param_value)

    else:
        return ''.join(random.choice(string.digits) for _ in range(int(config.DEFAULT_STRING_LENGTH)))


def dict_link(
        config: SynthTableConfig,
        param_name: str,
        param_value: str,
        **kwargs) -> str:

    import numpy as np
    import pandas as pd

    schema_name = param_value.split('.')[0]
    table_name = param_value.split('.')[1]
    column_name = param_value.split('.')[2]

    if param_name == 'DICT_LINK':
        dictionary = pd.read_csv(
            f"{config.DICTIONARY_DIR}/{schema_name}/{table_name}.txt",
            dtype=str,
            sep=';'
        )

        dict_values = dictionary[column_name].to_list()

        try:
            dict_values_probability = dictionary['VALUE_PROBABILITY'].fillna(0).astype(float).to_list()
        except KeyError:
            raise ValueError(f'META_COLUMN file does not contain column VALUE_PROBABILITY. DICTIONARY: {schema_name}.{table_name}')

        sum_values_probability = round(sum(dict_values_probability), 2)

        if sum_values_probability != 1:
            raise ValueError(f'Probability sum is not equal to 1 (sum = {sum_values_probability}). DICTIONARY: {schema_name}.{table_name}')


        random_dict_value = np.random.choice(
            a=dict_values,
            size=1,
            p=dict_values_probability)[0]

        if random_dict_value == 'nan':
            random_dict_value = None

        return random_dict_value


def active_flg(**kwargs) -> int:

    return 1


def current_dttm(**kwargs):
    from datetime import datetime
    now = datetime.now()
    return now.strftime("%Y-%m-%d %H:%M:%S")


def from_dttm(
        config: SynthTableConfig,
        min_effective_from_dttm: datetime.datetime = None,
        **kwargs) -> datetime.datetime:

    """

    Parameters
    ----------
    config: SynthTableConfig
    min_effective_from_dttm: datetime.datetime

    Returns
    -------
    datetime.datetime
    """

    from datetime import timedelta
    import random

    if not min_effective_from_dttm:
        min_effective_from_dttm = config.DEFAULT_MIN_EFFECTIVE_FROM_DTTM
    end_date = config.DEFAULT_MAX_EFFECTIVE_TO_DTTM
    from_datetime = min_effective_from_dttm + timedelta(seconds=random.randint(0, int((end_date - min_effective_from_dttm).total_seconds())))
    return from_datetime


def open_effective_to_dttm(
        config: SynthTableConfig,
        **kwargs) -> datetime.datetime:

    """

    Returns
    -------
    datetime.datetime
    """

    return config.DEFAULT_OPEN_VERSION_EFFECTIVE_TO_DTTM


def close_effective_to_dttm(
        config: SynthTableConfig,
        start_date: datetime.datetime = None,
        **kwargs) -> datetime.datetime:

    """

    Parameters
    ----------
    config: SynthTableConfig
    start_date: datetime.datetime

    Returns
    -------
    datetime.datetime
    """

    from datetime import timedelta
    import random

    if not start_date:
        start_date = config.DEFAULT_MIN_EFFECTIVE_FROM_DTTM
    end_date = config.DEFAULT_MAX_EFFECTIVE_TO_DTTM
    max_version_duration_sec = int((end_date - start_date).total_seconds())
    return start_date + timedelta(seconds=random.randint(0, max_version_duration_sec + 1))


def random_dttm(
        config: SynthTableConfig,
        min_birth_dttm: datetime.datetime = None,
        max_birth_dttm: datetime.datetime = None,
        **kwargs) -> datetime.datetime:
    """

    Parameters
    ----------
    config: SynthTableConfig
    min_birth_dttm: datetime.datetime
    max_birth_dttm: datetime.datetime

    Returns
    -------
    datetime.datetime
    """

    import random
    from datetime import timedelta

    if not min_birth_dttm or max_birth_dttm:
        min_birth_dttm = config.DEFAULT_MIN_BIRTH_DTTM
        max_birth_dttm = config.DEFAULT_MAX_BIRTH_DTTM
    return min_birth_dttm + timedelta(seconds=random.randint(0, int((max_birth_dttm - min_birth_dttm).total_seconds())))


def random_birth_dttm(
        config: SynthTableConfig,
        param_name: str,
        param_value: str,
        **kwargs) -> datetime.datetime:

    import random
    import json
    from datetime import timedelta

    if param_name == 'RANGE':
        min_birth_dttm, max_birth_dttm = json.loads(param_value)
    else:
        min_birth_dttm = config.DEFAULT_MIN_BIRTH_DTTM
        max_birth_dttm = config.DEFAULT_MAX_BIRTH_DTTM
    return min_birth_dttm + timedelta(seconds=random.randint(0, int((max_birth_dttm - min_birth_dttm).total_seconds())))


def random_string(
        config: SynthTableConfig,
        **kwargs) -> str:
    import random
    import string

    return ''.join(
        random.choice(
            string.ascii_uppercase + string.digits + string.ascii_lowercase
        ) for _ in range(config.DEFAULT_STRING_LENGTH)
    )


def random_float(
        config: SynthTableConfig,
        param_name: str,
        param_value: str,
        **kwargs) -> float:
    import random
    import json

    if param_name == 'RANGE':
        float_min_range, float_max_range = json.loads(param_value)
    else:
        float_min_range = config.DEFAULT_FLOAT_MIN_RANGE
        float_max_range = config.DEFAULT_FLOAT_MAX_RANGE

    return random.uniform(float_min_range, float_max_range)


def random_int(
        config: SynthTableConfig,
        param_name: str,
        param_value: str,
        **kwargs) -> int:
    import random
    import json

    if param_name == 'RANGE':
        int_min_range, int_max_range = json.loads(param_value)
    else:
        int_min_range = config.DEFAULT_INT_MIN_RANGE
        int_max_range = config.DEFAULT_INT_MAX_RANGE

    return random.randrange(int_min_range, int_max_range + 1)

