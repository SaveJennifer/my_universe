def _dict_processing(dictionary: dict) -> dict:
    import math
    for key in dictionary.keys():
        if not isinstance(dictionary[key], str):
            if math.isnan(dictionary[key]):
                dictionary[key] = None
        else:
            dictionary[key] = dictionary[key].upper()
    return dictionary


class MainConfig:
    def __init__(self):
        from datetime import datetime

        self.DEFAULT_INT_MIN_RANGE = 0
        self.DEFAULT_INT_MAX_RANGE = 100
        self.DEFAULT_FLOAT_MIN_RANGE = -10
        self.DEFAULT_FLOAT_MAX_RANGE = 10
        self.DEFAULT_STRING_LENGTH = 10
        self.DEFAULT_MIN_EFFECTIVE_FROM_DTTM = datetime.strptime('2018-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
        self.DEFAULT_MAX_EFFECTIVE_TO_DTTM = datetime.now()
        self.DEFAULT_MIN_BIRTH_DTTM = datetime.strptime('1958-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
        self.DEFAULT_MAX_BIRTH_DTTM = datetime.strptime('2004-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
        self.DEFAULT_OPEN_VERSION_EFFECTIVE_TO_DTTM = datetime.strptime('5999-12-31 23:59:59', '%Y-%m-%d %H:%M:%S')
        self.START_PROBABILITY_NUMBER = 0
        self.END_PROBABILITY_NUMBER = 3
        self.VERSION_PROBABILITY = range(self.START_PROBABILITY_NUMBER, self.END_PROBABILITY_NUMBER)

        self.RESULT_DIR = 'src/results'
        self.METADATA_DIR = 'src/metadata'
        self.DICTIONARY_DIR = f'{self.METADATA_DIR}/dictionaries'
        self.META_DICTIONARY_ALIAS = 'META_DICT'
        self.META_DICTIONARY_DIR = f'{self.DICTIONARY_DIR}/{self.META_DICTIONARY_ALIAS}'

        self.IMMUTABLE_SCD2_ROLES = ('TECH', 'PK')


class SynthTableConfig(MainConfig):

    def __init__(
            self,
            meta_table_dict: dict,
            meta_columns_dict: dict
    ):
        super().__init__()

        self.SCHEMA_NAME = meta_table_dict['SCHEMA_NAME'][0]
        self.TABLE_NAME = meta_table_dict['TABLE_NAME'][0]
        self.ROWS_NUMBER = meta_table_dict['ROWS_NUMBER'][0]

        self.COLUMN_NUM = len(meta_columns_dict['COLUMN_NAME'].keys())
        self.COLUMN_NAME_DICT = _dict_processing(meta_columns_dict['COLUMN_NAME'])
        self.COLUMN_TYPE_DICT = _dict_processing(meta_columns_dict['COLUMN_TYPE'])
        self.COLUMN_DATA_DOMAIN_DICT = _dict_processing(meta_columns_dict['COLUMN_DATA_DOMAIN'])
        self.PARAM_NAME_DICT = _dict_processing(meta_columns_dict['PARAM_NAME'])
        self.PARAM_VALUE_DICT = _dict_processing(meta_columns_dict['PARAM_VALUE'])
        self.COLUMN_ROLE_DICT = _dict_processing(meta_columns_dict['COLUMN_ROLE'])

        self.SCHEMA_RESULT_DIR = f'{self.RESULT_DIR}/{self.SCHEMA_NAME}'
        self.RESULT_FILEPATH = f'{self.SCHEMA_RESULT_DIR}/{self.TABLE_NAME}.txt'

        # Non-static
        self.CURRENT_GENERATE_ROW_NUMBER = None

