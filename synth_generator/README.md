### synth_generator
Генерация синтатических данных в формате csv. SCD2 версионнность.



### Инструкция

Перед запуском скрипта необходимо:

#### 1. Склонировать репозиторий.

	Скачать Git Bash. 
	
	Затем в веб-версии репозитория выбрать clone HTTP.
	
	Полученную ссылку использовать при git clone в Git Bash. 
	
	Например: git clone https://<ВАШ_ЛОГИН>@git.glowbyteconsulting.com/scm/vtb24/synth-generator.git
	   
	Важно: вы должны находиться в корпоративной сети Glowbyte. 
	   
	После выполнения команды, у вас на локальной машине должна появиться локальная копия репозитория. 
	   
	Расположение копии вы можете узнать, если выполните в Git Bash команду pwd.
	
#### 2. Вписать в вызов функции main в файле main.py schema_name и table_name.

#### 3. Создать в src/metadata/<schema_name>/<table_name> два файла: meta_columns, meta_table. 
   
#### 3.1. В meta_columns входит:

	COLUMN_NAME 			-- название поля
	COLUMN_TYPE 			-- возможные типы поля: int, string, timestamp
	COLUMN_TYPE_BUSINESS_LOGIC 	-- домен данных
	PARAM_TYPE 			-- тип параметра
	PARAM_VALUE 			-- конкретное значение для PARAM_TYPE
	FIELD_VALUE 			-- указатель на то, является ли поле ключом (PK) или техническим полем (TECH)

      
Возможные домены данных COLUMN_TYPE_BUSINESS_LOGIC:

* int:

    	pkrk -- уникальный ключ
 
* string:

    	strdigit -- строка, состоящая из цифр
		dict -- словарь, в котором находятся все возможные значения для поля
    	active_flg -- изначально равен 1, при создании новой версии становится равен 0

* timestamp:

    	from_dttm -- дата начала действия версии
    	to_dttm -- дата окончания версии актуальной версии
    	current_dttm -- текущая дата и время
    	random_dttm -- случайная дата и время
    	random_birth_dttm -- случайная дата и время рождения 

* float

Возможные типы параметров PARAM_TYPE:

	LENGTH -- длина строки 
	VALUE -- конкретное значение
	DICT_LINK -- название и нужная колонка в словаре, который нужно занести в директорию results
	META_DICT -- название и нужная колонка в словаре, который не нужно заносить в директорию results
	RANGE -- диапазон значений. Формат ввода значения: "[MIN, MAX]". Вводит с кавычками
	FUNCTION_VALUE -- значение, которое возвращает заданная функция
	DATETIME_RANGE -- диапазон datetime

Пример META_COLUMNS: *src/metadata/PROD_CDM/CD_CUSTOMER_RISK_LVL/META_COLUMNS.txt*

#### 3.2 В meta_table входит:

    schema_name -- имя схемы генерируемой таблицы
	table_name -- имя генерируемой таблицы
	row_number -- количество строк в генерируемой таблице.

Пример META_TABLE: *src/metadata/PROD_CDM/CD_CUSTOMER_RISK_LVL/META_TABLE.txt*
      

#### 4. Создать необходимые для генерации синтетических данных словари: 
	Если это META_DICT, внести в src/metadata/meta_dictionaries/<schema_name>/<dictionary_name>.txt 
	Если это DICT_LINK, внести в src/results/dictionaries/<schema_name>/<dictionary_name>.txt

Пример META_DICT: *src/metadata/meta_dictionaries/PROD_DDS/DIM_PARTY_TYPE.txt*

Пример DICT_LINK: *src/metadata/dictionaries/PROD_DDS/DIM_RISK_LEVEL_MDM.txt*

#### 5. Запустить скрипт main.py

