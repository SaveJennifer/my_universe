import pandas as pd
from config import SynthTableConfig


def generate_table(
        synth_cfg: SynthTableConfig,
        schema_name: str,
        table_name: str) -> pd.DataFrame:

    """

    Parameters
    ----------
    synth_cfg : SynthTableConfig
    schema_name: str
    table_name: str
    Returns
    -------
    pd.DataFrame

    """

    import pandas as pd
    from src.lib.rows_generator import generate_row, close_version, generate_new_version
    import random
    from tqdm import tqdm


    synth_table = []

    for row_number in tqdm(range(synth_cfg.ROWS_NUMBER)):
        synth_cfg.CURRENT_GENERATE_ROW_NUMBER = row_number
        row = generate_row(
            config=synth_cfg
        )

        synth_table.append(row)
        for _ in synth_cfg.VERSION_PROBABILITY:
            if random.randint(synth_cfg.START_PROBABILITY_NUMBER, synth_cfg.END_PROBABILITY_NUMBER) == 0:
                previous_row_from_the_table = synth_table.pop()

                # Close old version
                previous_row_update = close_version(
                    row=previous_row_from_the_table.copy(),
                    config=synth_cfg
                )
                synth_table.append(previous_row_update)

                # Generate new version based old version
                current_row_update = generate_new_version(
                    row_previous_version=previous_row_update.copy(),
                    config=synth_cfg
                )
                synth_table.append(current_row_update)

    table_df = pd.DataFrame(synth_table)
    table_df.columns = [synth_cfg.COLUMN_NAME_DICT[column_id] for column_id in range(synth_cfg.COLUMN_NUM)]

    return table_df