from src.lib.table_generator import generate_table
import pandas as pd
from config import MainConfig
import os


def main(schema_name: str,
         table_name: str):
    cfg = MainConfig()

    params_csv_meta_columns = pd.read_csv(r'{METADATA_DIR}/{schema_name}/{table_name}/META_COLUMNS.txt'.format(
        METADATA_DIR=cfg.METADATA_DIR,
        schema_name=schema_name,
        table_name=table_name),
        sep=';')
    params_csv_meta_table = pd.read_csv(r'{METADATA_DIR}/{schema_name}/{table_name}/META_TABLE.txt'.format(
        METADATA_DIR=cfg.METADATA_DIR,
        schema_name=schema_name,
        table_name=table_name),
        sep=';')
    schema_name = params_csv_meta_table['SCHEMA_NAME'][0]
    table_name = params_csv_meta_table['TABLE_NAME'][0]

    if not os.path.exists(cfg.RESULT_FILEPATH.format(schema_name=schema_name)):
        os.mkdir(cfg.RESULT_FILEPATH.format(schema_name=schema_name))

    generate_table(params_csv_meta_columns, params_csv_meta_table, schema_name, table_name).to_csv(
        f"{cfg.RESULT_DIR}/{schema_name}/{table_name}.txt",
        encoding='utf-8',
        index=False,
        sep=';')


main(schema_name='PROD_REPL_ODS_SPECTRUM', table_name='H2_CLIENT_EXT')
