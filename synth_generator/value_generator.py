import datetime
import pandas as pd
from typing import Any, Optional


def generate_int(
        column_type_business_logic: str,
        row_number: int,
        param_type: str,
        param_value: Any,
        schema_name: str,
        table_name: str
        ) -> int:

    """

    Parameters
    ----------
    column_type_business_logic: pd.DataFrame
    row_number: int
    param_type: str
    param_value: int
    schema_name: str
    table_name: str
    Returns
    -------
    int
    """
    from config import MainConfig
    import random
    from src.lib.business_logic_mapper import pkrk
    import json

    cfg = MainConfig()

    if param_type == 'RANGE':
        int_min_range, int_max_range = json.loads(param_value)
    else:
        int_min_range = cfg.INT_MIN_RANGE
        int_max_range = cfg.INT_MAX_RANGE

    generate_int_mapping = {
        'pkrk': pkrk(row_number),
        None: random.randrange(int_min_range, int_max_range + 1)
    }
    return generate_int_mapping[column_type_business_logic]


def generate_string(column_type_business_logic: str,
                    row_number: int,
                    param_type: str,
                    param_value: int,
                    schema_name: str,
                    table_name: str
                    ) -> str:
    """

    Parameters
    ----------
    column_type_business_logic: str
    row_number: int
    param_type: str
    param_value: int
    schema_name: str
    table_name: str
    Returns
    -------
    str
    """

    import random
    import string
    from src.lib.business_logic_mapper import strdigit, dict_link, active_flg
    from config import MainConfig

    cfg = MainConfig()

    generate_str_mapping = {
        'strdigit': strdigit(param_type, param_value),
        'dict': dict_link(param_type, param_value),
        'active_flg': active_flg(),
        None: ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(cfg.LENGTH))
    }
    return generate_str_mapping[column_type_business_logic]


def generate_timestamp(column_type_business_logic: str,
                       row_number: int,
                       param_type: str,
                       param_value: str,
                       schema_name: str,
                       table_name: str
                       ) -> datetime.datetime:
    """

    Parameters
    ----------
    column_type_business_logic: str
    row_number: int
    param_type: str
    param_value: str
    schema_name: str
    table_name: str
    Returns
    -------
    datetime.datetime
    """
    from src.lib.business_logic_mapper import from_dttm, to_dttm, extra_to_dttm, random_dttm, current_dttm, random_birth_dttm

    generate_timestamp_mapping = {
        'from_dttm': from_dttm(),
        'to_dttm': to_dttm(),
        'extra_to_dttm': extra_to_dttm(),
        'current_dttm': current_dttm(),
        'random_dttm': random_dttm(),
        'random_birth_dttm': random_birth_dttm(),
        None: random_dttm()
    }
    return generate_timestamp_mapping[column_type_business_logic]


def generate_float(
        column_type_business_logic: Optional[str],
        row_number: int,
        param_type: str,
        param_value: Any,
        schema_name: str,
        table_name: str) -> float:


    from config import MainConfig
    import random
    import json

    cfg = MainConfig()

    if param_type == 'RANGE':
        float_min_range, float_max_range = json.loads(param_value)
    else:
        float_min_range = cfg.FLOAT_MIN_RANGE
        float_max_range = cfg.FLOAT_MAX_RANGE

    generate_float_mapping = {
        None: random.uniform(float_min_range, float_max_range)
    }

    return generate_float_mapping[column_type_business_logic]


# TODO
def generate_value(column_type: str,
                   column_type_business_logic: str,
                   row_number: int,
                   param_type: str,
                   param_value: datetime.datetime,
                   schema_name: str,
                   table_name: str):
    """

    Parameters
    ----------
    column_type:str
    column_type_business_logic:str
    row_number:int
    param_type:str
    param_value:datetime.datetime
    schema_name: str
    table_name: str

    Returns
    -------
    int or str or datetime.datetime
    """
    type_mapping = {
        'int': generate_int,
        'string': generate_string,
        'timestamp': generate_timestamp,
        'float': generate_float
    }
    return type_mapping[column_type](column_type_business_logic, row_number, param_type, param_value, schema_name, table_name)







