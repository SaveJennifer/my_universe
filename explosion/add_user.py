import sys
import sqlite3
import hashlib


connection = sqlite3.connect("users.sqlite")
cursor = connection.cursor()

login, password = sys.argv[1:]

user_id = cursor.execute("SELECT MAX(id) FROM users").fetchone()[0] or 0
hashed_password = hashlib.sha512(bytes(password, "utf8")).hexdigest()

cursor.execute("INSERT INTO users VALUES(?, ?, ?)", (user_id + 1, login, hashed_password))

connection.commit()
connection.close()
