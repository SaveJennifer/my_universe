import sqlite3


connection = sqlite3.connect("users.sqlite")
cursor = connection.cursor()

cursor.execute("CREATE TABLE IF NOT EXISTS users (id INT PRIMARY KEY, login TEXT UNIQUE, hashed_password TEXT)")
        
connection.commit()
connection.close()
