import sys
import sqlite3
import hashlib

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox


ERROR_MESSAGES = {
    "probability_of_fire": "вероятность возникновения пожароопасного режима должна быть вещественным числом",
    "probability_of_current_parameter": "вероятность того, что значение параметра лежит в диапазоне пожароопасных значений должна быть вещественным числом",
    "probability_of_protection_failure": "вероятность несрабатывания аппарата защиты должна быть вещественным числом",
    "probability_of_ignition": "вероятность воспламенения должна быть вещественным числом"
}


def error_handler(error_text):
    def decorated_func(func):
        def wrapped(*args, **kwargs):
            result = func(*args, **kwargs)
            if result is None:
                message = QMessageBox()
                message.setIcon(QMessageBox.Critical)
                message.setWindowTitle("Error")
                message.setText(error_text)
                message.exec_()
            return result
        
        return wrapped
    
    return decorated_func


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi(r"ui/window.ui", self)
        self.setFixedSize(self.size())

        self.calc.clicked.connect(self.calculate)

    def calculate(self) -> None:
        q1 = self._get_probability_of_fire()
        q2 = self._get_probability_of_current_parameter()
        q3 = self._get_probability_of_protection_failure()
        q4 = self._get_probability_of_ignition()

        if not all([q1, q2, q3, q4]):
            return
        
        result = round(q1 * q2 * q3 * q4, 13)
        self.result.setText(str(result))
        if result <= 10**-6:
            self.admissibility.setText("Допустимо")
            self.admissibility.setStyleSheet("color: rgb(43, 255, 32); font: 10pt \"Corbel\";")
        else:
            self.admissibility.setText("Недопустимо")
            self.admissibility.setStyleSheet("color: rgb(255, 0, 0); font: 10pt \"Corbel\";")

    @error_handler(ERROR_MESSAGES["probability_of_fire"])
    def _get_probability_of_fire(self) -> any:
        return self.__to_float(self.q1.text().strip())
    
    @error_handler(ERROR_MESSAGES["probability_of_current_parameter"])
    def _get_probability_of_current_parameter(self) -> any:
        return self.__to_float(self.q2.text().strip())
    
    @error_handler(ERROR_MESSAGES["probability_of_protection_failure"])
    def _get_probability_of_protection_failure(self) -> any:
        return self.__to_float(self.q3.text().strip())
    
    @error_handler(ERROR_MESSAGES["probability_of_ignition"])
    def _get_probability_of_ignition(self) -> any:
        return self.__to_float(self.q4.text().strip())
    
    @staticmethod
    def __to_float(string: str) -> any:
        try:
            return float(string)
        except:
            return


class LogInWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi(r"ui/log_in.ui", self)
        self.setFixedSize(self.size())
        
        self.is_entered = False

        self.error_line.hide()

        self.connection = sqlite3.connect(r"users.sqlite")
        self.cursor = self.connection.cursor()

        self.log_in_button.clicked.connect(self.data_validation)

    def data_validation(self):
        self.error_line.hide()

        login = self.login.text()
        password = self.password.text()

        if login.isspace() or password.isspace() or not login or not password:
            self.show_error("Поля логина и пароля должны быть заполнены")
            return
        
        hashed_password = self.cursor.execute(f"SELECT hashed_password FROM users WHERE login = '{login}'").fetchone()
        if hashed_password is not None and hashed_password[0] == hashlib.sha512(bytes(password, "utf8")).hexdigest():
            self.is_entered = True
            self.close()
        
        self.show_error("Неправильный логин или пароль")
        
    def show_error(self, error):
        self.error_line.setText(error)
        self.error_line.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = LogInWindow()
    ex.show()
    app.exec()

    if ex.is_entered:
        ex = MainWindow()
        ex.show()
        sys.exit(app.exec())
