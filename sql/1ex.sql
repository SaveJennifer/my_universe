CREATE TABLE customers(
    cust_id INT NOT NULL,
    cust_name VARCHAR(40)
    );
INSERT INTO customers (cust_id, cust_name)
VALUES ('1','Ivanov M.Y.');
INSERT INTO customers (cust_id, cust_name)
VALUES ('2', 'Sidorova E.V.');
INSERT INTO customers (cust_id, cust_name)
VALUES ('3', 'Grishaev N.L.');
INSERT INTO customers (cust_id, cust_name)
VALUES ('4', 'Bazarov F.S.');
INSERT INTO customers (cust_id, cust_name)
VALUES ('5', 'Leontiev A.P.');
SELECT *
FROM customers;



CREATE TABLE stores(
    store_id INT NOT NULL,
    store_name VARCHAR(40),
    store_adress VARCHAR(50)
    );
INSERT INTO stores (store_id, store_name, store_adress)
VALUES ('1','Pyterochka', 'Tverskaya 9');
INSERT INTO stores (store_id, store_name, store_adress)
VALUES ('2','Perekrestok', 'Angarskaya 11');
INSERT INTO stores (store_id, store_name, store_adress)
VALUES ('3','Magnit', 'Taganskaya 3');
SELECT *
FROM stores;


CREATE TABLE transactions(
    trans_id INT NOT NULL,
    cust_id INT NOT NULL,
    store_id INT NOT NULL,
    trans_amt NUMBER,
    trans_dttm TIMESTAMP
    );
INSERT INTO transactions (trans_id, cust_id, store_id, trans_amt, trans_dttm)
VALUES ('1', '3', '2', '3000', TO_TIMESTAMP('2020-03-23 12:01:17', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO transactions (trans_id, cust_id, store_id, trans_amt, trans_dttm)
VALUES ('2', '1', '1', '5000', TO_TIMESTAMP('2021-02-11 17:19:24', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO transactions (trans_id, cust_id, store_id, trans_amt, trans_dttm)
VALUES ('3', '2', '3', '7000', TO_TIMESTAMP('2019-03-03 06:02:55', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO transactions (trans_id, cust_id, store_id, trans_amt, trans_dttm)
VALUES ('4', '1', '3', '4000', TO_TIMESTAMP('2022-03-13 07:02:45', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO transactions (trans_id, cust_id, store_id, trans_amt, trans_dttm)
VALUES ('5', '2', '2', '4500', TO_TIMESTAMP('2022-02-03 04:02:55', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO transactions (trans_id, cust_id, store_id, trans_amt, trans_dttm)
VALUES ('6', '3', '1', '8500', TO_TIMESTAMP('2020-01-02 14:02:13', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO transactions (trans_id, cust_id, store_id, trans_amt, trans_dttm)
VALUES ('7', '2', '1', '4200', TO_TIMESTAMP('2019-01-11 08:02:36', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO transactions (trans_id, cust_id, store_id, trans_amt, trans_dttm)
VALUES ('8', '3', '3', '9800', TO_TIMESTAMP('2022-02-23 02:04:52', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO transactions (trans_id, cust_id, store_id, trans_amt, trans_dttm)
VALUES ('9', '1', '2', '6400', TO_TIMESTAMP('2022-03-23 18:07:17', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO transactions (trans_id, cust_id, store_id, trans_amt, trans_dttm)
VALUES ('10', '4', '2', '15000', TO_TIMESTAMP('2020-03-23 13:02:19', 'YYYY-MM-DD HH24:MI:SS'));
SELECT * 
FROM transactions;



CREATE TABLE client_sum(
    cust_id INT NOT NULL,
    trans_sum NUMBER,
    eff_from_dttm TIMESTAMP,
    eff_to_dttm TIMESTAMP
    );
INSERT INTO client_sum (cust_id, trans_sum, eff_from_dttm, eff_to_dttm)
SELECT 
  cust_id, 
  SUM(trans_amt) OVER (PARTITION BY cust_id ORDER BY trans_dttm) AS trans_sum, 
  trans_dttm as eff_from_dttm, 
  CASE 
    WHEN lead(trans_dttm) OVER (PARTITION BY cust_id ORDER BY trans_dttm) IS NOT NULL 
    THEN lead(trans_dttm) OVER (PARTITION BY cust_id ORDER BY trans_dttm) - INTERVAL '1' SECOND
  ELSE TO_TIMESTAMP('5999-12-31 00:00:00', 'YYYY-MM-DD HH24:MI:SS') END eff_to_dttm
FROM transactions
UNION ALL
SELECT 
  cust_id, 
  0 trans_sum, 
  TO_TIMESTAMP('1900-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS') eff_from_dttm, 
  MIN(trans_dttm) - INTERVAL '1' SECOND eff_to_dttm
FROM transactions
GROUP BY cust_id;

SELECT 
  cust_id, 
  trans_sum, 
  eff_from_dttm, 
  eff_to_dttm
FROM client_sum
ORDER BY cust_id, eff_from_dttm;




CREATE TABLE client_phone(
    cust_id INT NOT NULL,
    cust_phone VARCHAR(30),
    eff_from_dttm TIMESTAMP,
    eff_to_dttm TIMESTAMP
    );
INSERT INTO client_phone (cust_id, eff_from_dttm, eff_to_dttm)
VALUES ('1', TO_TIMESTAMP('1900-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2019-03-01 12:10:16', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO client_phone (cust_id, eff_from_dttm, eff_to_dttm)
VALUES ('2', TO_TIMESTAMP('1900-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2018-10-01 13:20:56', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO client_phone (cust_id, eff_from_dttm, eff_to_dttm)
VALUES ('3', TO_TIMESTAMP('1900-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2019-02-09 14:15:36', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO client_phone (cust_id, cust_phone, eff_from_dttm, eff_to_dttm)
VALUES ('1', '89853332294', TO_TIMESTAMP('2019-03-01 12:10:17', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2022-01-01 15:01:12', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO client_phone (cust_id, cust_phone, eff_from_dttm, eff_to_dttm)
VALUES ('2', '89035521236', TO_TIMESTAMP('2018-10-01 13:20:57', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2021-01-05 13:04:59', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO client_phone (cust_id, cust_phone, eff_from_dttm, eff_to_dttm)
VALUES ('3', '89099623349', TO_TIMESTAMP('2019-02-09 14:15:37', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2021-11-02 11:11:32', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO client_phone (cust_id, cust_phone, eff_from_dttm, eff_to_dttm)
VALUES ('1', '89853312234', TO_TIMESTAMP('2022-01-01 15:01:13', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2022-03-20 18:12:32', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO client_phone (cust_id, cust_phone, eff_from_dttm, eff_to_dttm)
VALUES ('2', '89058321032', TO_TIMESTAMP('2021-01-05 13:05:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2022-02-23 16:41:32', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO client_phone (cust_id, cust_phone, eff_from_dttm, eff_to_dttm)
VALUES ('3', '89653421314', TO_TIMESTAMP('2021-11-02 11:11:32', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2022-01-23 15:01:12', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO client_phone (cust_id, cust_phone, eff_from_dttm, eff_to_dttm)
VALUES ('1', '89033543212', TO_TIMESTAMP('2022-03-20 18:12:33', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('5999-12-31 00:00:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO client_phone (cust_id, cust_phone, eff_from_dttm, eff_to_dttm)
VALUES ('2', '89853401234', TO_TIMESTAMP('2022-02-23 16:41:33', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('5999-12-31 00:00:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO client_phone (cust_id, cust_phone, eff_from_dttm, eff_to_dttm)
VALUES ('3', '89099995316', TO_TIMESTAMP('2022-01-23 15:01:13', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('5999-12-31 00:00:00', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO client_phone (cust_id, cust_phone, eff_from_dttm, eff_to_dttm)
VALUES ('5', '89095033212', TO_TIMESTAMP('2022-03-21 11:13:53', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('5999-12-31 00:00:00', 'YYYY-MM-DD HH24:MI:SS'));

SELECT *
FROM client_phone;

CREATE TABLE client_sum_dttm(
    cust_id INT,
    cust_phone VARCHAR(30),
    trans_sum NUMBER,
    eff_from_dttm TIMESTAMP,
    eff_to_dttm TIMESTAMP
    );
INSERT INTO client_sum_dttm (cust_id, cust_phone, trans_sum, eff_from_dttm, eff_to_dttm)
SELECT 
  CASE
    WHEN cp.cust_id IS NOT NULL THEN cp.cust_id
    ELSE cs.cust_id
  END as cust_id, 
  cp.cust_phone, 
  cs.trans_sum,
  CASE
    WHEN cs.eff_from_dttm IS NULL THEN cp.eff_from_dttm
    WHEN cp.eff_from_dttm > cs.eff_from_dttm then cp.eff_from_dttm
    ELSE cs.eff_from_dttm
  END eff_from_time,
  CASE
    WHEN cs.eff_to_dttm IS NULL THEN cp.eff_to_dttm
    WHEN cp.eff_to_dttm < cs.eff_to_dttm then cp.eff_to_dttm
    ELSE cs.eff_to_dttm
END eff_to_time
FROM client_phone cp
FULL OUTER JOIN client_sum cs
ON cp.cust_id = cs.cust_id
  AND cs.eff_from_dttm < cp.eff_to_dttm
  AND cs.eff_to_dttm > cp.eff_from_dttm
ORDER BY cp.cust_id, cp.eff_from_dttm, cs.eff_from_dttm;

SELECT *
FROM client_sum_dttm
ORDER BY cust_id