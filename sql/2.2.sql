CREATE TABLE cs1(
    cust_id INT,
    cust_name VARCHAR(40)
    );
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('1','Ivanov M.Y.');
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('2', 'Sidorova E.V.');
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('3', 'Grishaev N.L.');
INSERT INTO cs1 (cust_id, cust_name)
VALUES (NULL, 'Bazarov N.V.');
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('5', NULL);
SELECT *
FROM cs1;

CREATE TABLE cs2(
    cust_id INT,
    cust_name VARCHAR(40)
    );
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('1','Ivanov M.Y.');
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('2', 'Sidorova E.V.');
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('3', 'Grishaev N.L.');
INSERT INTO cs2 (cust_id, cust_name)
VALUES (NULL, 'Bazarov N.A.');
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('5', NULL);
SELECT *
FROM cs2;


SELECT
CASE
  WHEN COUNT(*) > 0 THEN 'Tables are different'
  ELSE 'Tables are identical'
END difference
FROM (
  SELECT *
  FROM (
    SELECT cs1.cust_id, 
    cs1.cust_name, 
    COUNT(*) as countall 
    FROM cs1
    GROUP BY cust_id, cust_name
  ) cs11
  FULL OUTER JOIN (
    SELECT cs2.cust_id,
      cs2.cust_name, 
      COUNT(*) as countall 
      FROM cs2 
      GROUP BY cs2.cust_id, cs2.cust_name
  ) cs22
  ON (cs11.cust_id = cs22.cust_id OR cs11.cust_id IS NULL AND cs22.cust_id IS NULL)
    AND (cs11.cust_name IS NULL AND cs22.cust_name IS NULL 
    OR cs11.cust_name = cs22.cust_name)
  WHERE ((cs22.cust_name IS NULL AND cs22.cust_id IS NULL 
    AND (cs11.cust_name IS NOT NULL OR cs11.cust_id IS NOT NULL)) OR (cs11.cust_name IS NULL AND cs11.cust_id IS NULL
    AND (cs22.cust_name IS NOT NULL OR cs22.cust_id IS NOT NULL))
    OR (cs11.countall <> cs22.countall))
)