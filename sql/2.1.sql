CREATE TABLE cs1(
    cust_id INT NOT NULL,
    cust_name VARCHAR(40)
    );
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('1','Ivanov M.Y.');
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('2', 'Sidorova E.V.');
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('3', 'Grishaev N.L.');
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('4', 'Bazarov F.S.');
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('5', NULL);
SELECT *
FROM cs1;

CREATE TABLE cs2(
    cust_id INT NOT NULL,
    cust_name VARCHAR(40)
    );
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('1','Ivanov M.Y.');
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('2', 'Sidorova E.V.');
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('3', 'Grishaev N.L.');
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('4', 'Bazarov F.A.');
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('5', NULL);
SELECT *
FROM cs2;


SELECT
CASE
  WHEN COUNT(*) > 0 THEN 'Tables are different'
  ELSE 'Tables are identical'
END difference
FROM (
SELECT *
FROM cs1
FULL OUTER JOIN cs2
ON cs1.cust_id = cs2.cust_id
WHERE (cs2.cust_id IS NULL OR cs1.cust_id IS NULL) OR cs1.cust_name <> cs2.cust_name 
OR cs1.cust_name IS NULL AND cs2.cust_name IS NOT NULL
OR cs2.cust_name IS NULL AND cs1.cust_name IS NOT NULL
)