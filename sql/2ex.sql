CREATE TABLE cs1(
    cust_id INT NOT NULL,
    cust_name VARCHAR(40)
    );
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('1','Ivanov M.Y.');
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('2', 'Sidorova E.V.');
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('3', 'Grishaev N.L.');
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('4', 'Bazarov F.S.');
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('5', NULL);
INSERT INTO cs1 (cust_id, cust_name)
VALUES ('6', 'Leontiev S.V.');
SELECT *
FROM cs1;

CREATE TABLE cs2(
    cust_id INT NOT NULL,
    cust_name VARCHAR(40)
    );
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('1','Ivanov M.Y.');
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('2', 'Sidorova E.V.');
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('3', 'Grishaev N.L.');
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('4', 'Bazarov F.S.');
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('5', NULL);
INSERT INTO cs2 (cust_id, cust_name)
VALUES ('7', 'Zabolotniy S.F.');

SELECT *
FROM cs2;

SELECT
SUM(CASE 
  WHEN (cust_id1 IS NULL AND cust_id2 IS NOT NULL) 
    OR (cust_id1 IS NOT NULL AND cust_id2 IS NULL)
  THEN 1
  ELSE 0
END) differenceofcust_id,
SUM(CASE 
  WHEN (cust_name1 <> cust_name2) 
    OR (cust_name1 IS NULL AND cust_name2 IS NOT NULL) 
    OR (cust_name1 IS NOT NULL AND cust_name2 IS NULL)
  THEN 1 
  ELSE 0
END) differenceofcust_name
FROM (
  SELECT cs1.cust_id as cust_id1, cs1.cust_name as cust_name1, cs2.cust_id as cust_id2, cs2.cust_name as cust_name2
  FROM cs1
  FULL OUTER JOIN cs2
  ON cs1.cust_id = cs2.cust_id
  WHERE (cs2.cust_id IS NULL OR cs1.cust_id IS NULL) 
    OR ((cs1.cust_name <> cs2.cust_name)
      OR (cs1.cust_name IS NULL AND cs2.cust_name IS NOT NULL)
      OR (cs2.cust_name IS NULL AND cs1.cust_name IS NOT NULL))
)