
CREATE TABLE UAT_DM_AML_WRK.FSC_PARTY_DIM_HASH_TEST_DAPP_1_DIFF AS
SELECT 
	DAPP_HASH.PARTY_NUMBER,
	DAPP_HASH.hash_value
FROM UAT_DM_AML_WRK.FSC_PARTY_DIM_HASH_TEST_DAPP_1_DAPP_HASH AS DAPP_HASH
LEFT JOIN UAT_DM_AML_WRK.FSC_PARTY_DIM_HASH_TEST_DRP_1_DRP_HASH AS DRP_HASH
ON 1=1
	AND DAPP_HASH.PARTY_NUMBER = DRP_HASH.PARTY_NUMBER
WHERE 1=0