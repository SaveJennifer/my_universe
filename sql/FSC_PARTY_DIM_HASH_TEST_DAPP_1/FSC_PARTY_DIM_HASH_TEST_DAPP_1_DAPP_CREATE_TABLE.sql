CREATE TABLE UAT_DM_AML_WRK.FSC_PARTY_DIM_HASH_TEST_DAPP_1_DAPP_HASH AS
SELECT
	PARTY_NUMBER,
	MD5(CONCAT(
		COALESCE(REPLACE(party_id,'#',''),''),'#',
		COALESCE(REPLACE(party_type_desc,'#',''),''),'#',
		COALESCE(REPLACE(party_tax_id,'#',''),''),'#',
		COALESCE(REPLACE(party_tax_id_type_code,'#',''),''),'#',
		COALESCE(REPLACE(party_id_state_code,'#',''),'')
	)
) AS hash_value FROM UAT_DM_AML_WRK.FSC_PARTY_DIM_HASH_TEST_DAPP_1
WHERE 1 = 0