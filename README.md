## Welcome to My Universe

There are few of my scripts, which I think could help you to rate my skills as a developer. Enjoy it:)

#### 1. check_hash
    The project, which helps to move tables from one database management system to another by creating hash values and comparing hash tables.
    It consists of two main scripts: create_hash_table.py and compare_hash_tables.py
    Also I've attached wrapper.py, templates.py, config.py and etc.

#### 2. synth-generator
    That's the project, which creates the synthetic data as .csv file.
    In data_domains.py you can check out the whole business logics for values.

#### 3. parse_html
    A small script, which parse sites. 
    Just shows that I can use Tkinter library.
    P.S: habr.csv is heavy enough. 
    
#### 4. send_offer
    The script, which would help me to get an offer:)
    Try .exe first

