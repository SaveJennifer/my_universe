import requests
import csv
from bs4 import BeautifulSoup
from fake_useragent import UserAgent


def main():
    try:
        with open(r'C:\Users\nikolay.azarov\Desktop\TEST\habr.csv', 'r') as csv_file:
            reader = csv.reader(csv_file)
            header = next(reader)
            for url in reader:
                page = requests.get(url[0], headers={'User-Agent': UserAgent().chrome})
                short_url = url[0].split('.com/')[1]
                soup = BeautifulSoup(page.text, 'html.parser')
                name = soup.title.text.split(' (')[0]

                with open(r'C:\Users\nikolay.azarov\Desktop\TEST\results\{short_url}.txt'.format(short_url=short_url),
                          'w', encoding="utf-8") as f:
                    f.write("{url}\n{name}\n{page}".format(url=url[0], page=page.text, name=name))
    except Exception as exception:
        print(exception)


if __name__ == "__main__":
    main()

